﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour {


	bool player1in;
	bool player2in;
	public float CoolNextLevel;

	//public GameObject highscore;
	//private TimerText otherScriptToAccess;

	//private bool booleanScript2;
	//private int intScript2;

	void Start()
	{
		//otherScriptToAccess = highscore.GetComponent<TimerText> ();

		//booleanScript2 = otherScriptToAccess.myBoolVar;

		//intScript2 = otherScriptToAccess.myIntVar;

		//Debug.Log (intScript2);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player2") {
			player2in = true;
		}
			
		if (other.tag == "Player1") {
			player1in = true;
		}

		if (player1in == true && player2in == true) {
			GameObject.Find ("Players").SendMessage ("Finish");


			//yield return new WaitForSeconds (5);
			Application.LoadLevel ((int)CoolNextLevel);
			return;
		}
	}
}