﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameController : MonoBehaviour {


	public TextMeshProUGUI restartText;
		//public GUIText gameOverText;
	public TextMeshProUGUI gameOverText;

		private bool gameOver;
		private bool restart;

		//private float timeLeft = 0.00f;
		//public GameObject timeLeftUI;
		//public GUIText highScore;

	//public GUIText timerText;
	//private float startTime;
	//private bool finished = false;



		void Start ()
		{
			//startTime = Time.time;
			gameOver = false;
			restart = false;
			restartText.text = "";
			gameOverText.text = "";
			//highScore.text = PlayerPrefs.GetInt ("Highscore", 1000).ToString ();
		}

	void Update (){
		//if (finished)
			//return;
		//float t = Time.time - startTime;
		//string minutes = ((int)t / 60).ToString ();
		//string seconds = (t % 60).ToString ("f2");
		//timerText.text = minutes + ":" + seconds;

		//timeLeft += Time.deltaTime;
		//timeLeftUI.gameObject.GetComponent<Text> ().text = ("Time: " + (int)timeLeft);

		//if (timeLeft < PlayerPrefs.GetInt ("HighScore")) {
		//PlayerPrefs.SetInt ("Highscore", (int)timeLeft);
		//highScore.text = ("HighScore: " + timeLeft.ToString ());
		//}
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
			}
		}
	}
		
	//public void Finish()
	//{
		//finished = true;
		//timerText.color = Color.red;
	//}


		public void GameOver ()
		{
			gameOverText.text = "Game Over!";
			gameOver = true;
			restartText.text = "Press 'R' for Restart";
			restart = true;
			
		}
}