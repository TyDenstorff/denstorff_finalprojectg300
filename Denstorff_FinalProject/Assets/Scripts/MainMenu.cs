﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {
	public float MMCoolNextLevel;

	public void PlayGame()
	{
		Application.LoadLevel ((int)MMCoolNextLevel);
	}

	public void MainMenuLevel()
	{
		Application.LoadLevel (0);
	}

	public void QuitGame ()
	{
		Debug.Log ("Quit");
		Application.Quit ();
	}
}