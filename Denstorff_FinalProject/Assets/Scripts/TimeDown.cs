﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDown : MonoBehaviour {

	private float timeLeft = 0.00f;
	public GameObject timeLeftUI;
	public Text highScore;

	//private float startTime; 
	// Use this for initialization
	void Start () {
		highScore.text = PlayerPrefs.GetInt ("Highscore", 1000).ToString ();

		//startTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
		timeLeft += Time.deltaTime;
		timeLeftUI.gameObject.GetComponent<Text> ().text = ("Time: " + (int)timeLeft);

		if (timeLeft < PlayerPrefs.GetInt ("HighScore")) 
		{
			PlayerPrefs.SetInt ("Highscore", (int)timeLeft);
			highScore.text = ("HighScore: " + timeLeft.ToString());
		}


		//float t = Time.time - startTime;
		//timerTextUI.gameObject.GetComponent<Text>().text = ("TimerLeft: " + (int)timerText);
		//string minutes = ((int) t / 60).ToString();
		//string seconds = (t % 60).ToString("f2");

		//timerText.text = minutes + ":" + seconds;
	}
}